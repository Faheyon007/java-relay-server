public class Configuration
{
    public ServerConfiguration server1;
    public ServerConfiguration server2;

    public Configuration(ServerConfiguration server1, ServerConfiguration server2)
    {
        this.server1 = server1;
        this.server2 = server2;
    }
}
