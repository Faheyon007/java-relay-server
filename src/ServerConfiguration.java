public class ServerConfiguration
{
    public String ip;
    public int port;

    public ServerConfiguration(String ip, int port)
    {
        this.ip = ip;
        this.port = port;
    }
}
