import java.io.*;
import java.net.Socket;





public class Server2ServerCommunicator
{
    private Configuration configuration;


    public Server2ServerCommunicator()
    {
        init();
    }

    public Server2ServerCommunicator(Configuration configuration)
    {
        this.configuration = configuration;

        init();
    }

    public Server2ServerCommunicator(ServerConfiguration server1, ServerConfiguration server2)
    {
        this.configuration = new Configuration(server1, server2);

        init();
    }

    public void init()
    {
        System.out.println("[ LOG ] : S2S init() has started");
        try
        (
                Socket client1 = new Socket(configuration.server1.ip, configuration.server1.port);
                Socket client2 = new Socket(configuration.server2.ip, configuration.server2.port);

                DataInputStream rx1 = new DataInputStream(client1.getInputStream());
                DataOutputStream tx1 = new DataOutputStream(client1.getOutputStream());

                DataInputStream rx2 = new DataInputStream(client2.getInputStream());
                DataOutputStream tx2 = new DataOutputStream(client2.getOutputStream())
        )
        {
            client1.setSoTimeout(450);
            client2.setSoTimeout(450);


            byte[] buffer;

            while (true)
            {
                // reading from client 1
                try
                {
                    buffer = new byte[rx1.available()];
                    rx1.read(buffer, 0, buffer.length);

                    if (buffer.length > 0)
                    {
                        tx2.write(buffer);
                        tx2.flush();
                        System.out.println("[ 1 ] ==>" + bytesToHex(buffer));
                    }
                }
                catch (Exception e)
                {
                }

                // reading from client 2
                try
                {
                    buffer = new byte[rx2.available()];
                    rx2.read(buffer, 0, buffer.length);

                    if (buffer.length > 0)
                    {
                        tx1.write(buffer);
                        tx1.flush();
                        System.out.println("[ 2 ] ==>" + bytesToHex(buffer));
                    }
                }
                catch (Exception e)
                {
                }

                Thread.sleep(100);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("[ LOG ] : S2S init() has completed");
    }

    public static String bytesToHex(final byte[] bytes) {
        final int HEX_SIZE = 3;
        final int MAX_BYTE_SIZE = 0xFF;
        // Return empty string if array is empty.
        if (bytes == null || bytes.length == 0) {
            return "";
        }
        final char[] hexArray = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
                '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        char[] hexChars = new char[bytes.length * HEX_SIZE];
        int tmp;
        for (int pos = 0; pos != bytes.length; ++pos) {
            tmp = bytes[pos] & MAX_BYTE_SIZE;
            hexChars[pos * HEX_SIZE] = hexArray[tmp >>> 4];
            hexChars[pos * HEX_SIZE + 1] = hexArray[tmp & 0x0F];
            hexChars[pos * HEX_SIZE + 2] = ' ';
        }
        return new String(hexChars, 0, hexChars.length - 1);
    }
}
