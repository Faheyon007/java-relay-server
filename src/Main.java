public class Main
{
    public static void main(String[] args)
    {
        if(args.length != 4)
        {
            System.out.println(String.format("[ %s ] : %s", "USAGE", "java -jar $JAR port tunnel-ip tunnel-port"));
            System.out.println(String.format("[ %s ] : %s", "EXAMPLE", "java -jar Server2ServerCommunicator.jar 8080 127.0.0.1 9090"));
            System.exit(-1);
        }

        String ipServer1 = args[0];
        int portServer1 = Integer.parseInt(args[1]);
        String ipServer2 = args[2];
        int portServer2 = Integer.parseInt(args[3]);


        System.out.println("--------- [ CONFIGURATION ] ---------");
        System.out.println(String.format("[ %s ] : %s", "SERVER 1 IP", ipServer1));
        System.out.println(String.format("[ %s ] : %s", "SERVER 1 PORT", portServer1));
        System.out.println(String.format("[ %s ] : %s", "SERVER 2 IP", ipServer2));
        System.out.println(String.format("[ %s ] : %s", "SERVER 2 PORT", portServer2));
        System.out.println("\n");

        Server2ServerCommunicator server = new Server2ServerCommunicator(new ServerConfiguration(ipServer1, portServer1), new ServerConfiguration(ipServer2, portServer2));

        System.out.println("--------- [ TERMINATED ] ---------");
    }
}
